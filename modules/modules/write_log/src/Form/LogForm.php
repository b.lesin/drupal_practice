<?php

namespace Drupal\write_log\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a WriteLog form.
 */
class LogForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'write_log_log';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];

    if(\Drupal::currentUser()->isAnonymous()) {
        $form['username'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Username'),
            '#required' => TRUE,
        ];

        $form['contact_info'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Anonymous users can provide contact information'),
        ];

        $form['phone'] = [
            '#type' => 'tel',
            '#title' => $this->t('Phone number'),
            '#states' => [
                'invisible' => [
                    ':input[name="contact_info"]' => ['checked' => FALSE],
                ]
            ]
        ];

        $form['mail'] = [
            '#type' => 'email',
            '#title' => $this->t("E-mail"),
            '#states' => [
                'invisible' => [
                    ':input[name="contact_info"]' => ['checked' => FALSE],
                ]
            ]
        ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('Thank you for response!'));
  }

}
